# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
@ScrollProcessor =
  process: ->
    return unless Modernizr.localstorage
    @restore() if location.hash == '#scroll'
    $(window).on 'scroll', => @onscroll()

  onscroll: (event) ->
    localStorage.setItem @key, $(window).scrollTop()

  restore: ->
    $(window).scrollTop localStorage.getItem(@key)

  key: "scroll#{location.pathname}"
# You can use CoffeeScript in this file: http://coffeescript.org/
