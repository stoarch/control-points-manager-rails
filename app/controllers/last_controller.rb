class LastController < ApplicationController
	before_filter :cors_check
#	after_filter :set_access_control_headers

  def cpmodes
		cpoints = Cpoint.all
		modes = {}
		cpoints.each do |cp|
			val = cp.cpmodes.last
			v = {min: val[:value_min], max: val[:value_max], caption: cp[:caption]}
			modes[cp[:code]] = v 
		end

		modes = Hash[modes.to_a.reverse]

    respond_to do |format|
			format.json { render status: 200, json: modes.to_json }.to_json
		end
  end


	def set_access_control_headers
		headers['Access-Control-Allow-Origin'] = '*'
		headers['Access-Control-Request-Methods'] = '*'
		headers['Access-Control-Allow-Headers'] = '*'
		headers['Access-Control-Max-Age'] = '1728000'
	end

	def cors_check
		if request.method == 'OPTIONS'
			headers['Acess-Control-Allow-Origin' ] = '*'
			headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE, OPTIONS'
			headers['Access-Control-Allow-Headers'] = 'X-Requested-With, X-Prototype-Version, Token'
			headers['Access-Control-Max-Age'] = '172800'

			render text: '', content_type: 'text/plain'
		end
	end
end
