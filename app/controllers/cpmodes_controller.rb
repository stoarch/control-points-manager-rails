class CpmodesController < ApplicationController
  before_action :set_cpmode, only: [:show, :edit, :update, :destroy]

  # GET /cpmodes
  # GET /cpmodes.json
  def index
    @cpoint = Cpoint.find(params[:cpoint_id])
    @cpmodes = @cpoint.cpmodes
  end

  # GET /cpmodes/1
  # GET /cpmodes/1.json
  def show
  end

  # GET /cpmodes/new
  def new
    @cpoint = Cpoint.find(params[:cpoint_id])
    @cpmode = @cpoint.cpmodes.build
  end

  # GET /cpmodes/1/edit
  def edit
  end

  # POST /cpmodes
  # POST /cpmodes.json
  def create
    @cpoint = Cpoint.find(params[:cpoint_id])
    @cpmode = @cpoint.cpmodes.build(cpmode_params)

    respond_to do |format|
      if @cpmode.save
				write_cp_mode_image()

        format.html { redirect_to cpoint_cpmodes_path( @cpoint ), notice: 'Cpmode was successfully created.' }
        format.json { render :show, status: :created, location: @cpmode }
      else
        format.html { render :new }
        format.json { render json: @cpmode.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cpmodes/1
  # PATCH/PUT /cpmodes/1.json
  def update
  	
    @cpoint = Cpoint.find(params[:cpoint_id])

    respond_to do |format|
      if @cpmode.update(cpmode_params)
				write_cp_mode_image()

        format.html { redirect_to cpoint_cpmodes_path( @cpoint ), notice: 'Cpmode was successfully updated.' }
        format.json { render :show, status: :ok, location: @cpmode }
      else
        format.html { render :edit }
        format.json { render json: @cpmode.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cpmodes/1
  # DELETE /cpmodes/1.json
  def destroy
    @cpmode.destroy
    respond_to do |format|
      format.html { redirect_to cpoint_cpmodes_path(@cpoint), notice: 'Cpmode was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
	
	MIN_VAL = 0
	MAX_VAL = 8

  private
		def write_cp_mode_image()
			canvas = Magick::ImageList.new
			image = canvas.new_image( 200, 200 ){ self.background_color = 'white' }

			yellow_angle_start = 135
			yellow_angle_end = yellow_angle_start + 270*@cpmode.value_min/MAX_VAL
			green_angle_end = yellow_angle_end + 270*(@cpmode.value_max - @cpmode.value_min)/MAX_VAL

			circle = Magick::Draw.new
			circle.stroke('red')
			circle.fill_opacity(0)
			circle.stroke_width(5)
			circle.ellipse( canvas.rows/2, canvas.columns/2, 73, 73, green_angle_end, 59 )
			circle.draw( canvas )

			circle = Magick::Draw.new
			circle.stroke('green')
			circle.fill_opacity(0)
			circle.stroke_width(5)
			circle.ellipse( canvas.rows/2, canvas.columns/2, 73, 73, yellow_angle_end, green_angle_end)
			circle.draw( canvas )

			circle = Magick::Draw.new
			circle.stroke('yellow')
			circle.fill_opacity(0)
			circle.stroke_width(5)
			circle.ellipse( canvas.rows/2, canvas.columns/2, 73, 73, yellow_angle_start, yellow_angle_end)
			circle.draw( canvas )

			circle = Magick::Draw.new
			circle.stroke('black')
			circle.fill_opacity(0)
			circle.stroke_width(1)
			circle.ellipse( canvas.rows/2, canvas.columns/2, 76, 76, 0, 360 )
			circle.draw( canvas )

			circle = Magick::Draw.new
			circle.stroke('black')
			circle.fill_opacity(0)
			circle.stroke_width(1)
			circle.ellipse( canvas.rows/2, canvas.columns/2, 70, 70, 0, 360 )
			circle.draw( canvas )

				txt = Magick::Draw.new

				image.annotate(txt, 0,0,55,150, "0"){
								txt.gravity = Magick::ForgetGravity
								txt.pointsize = 15
								txt.stroke = '#000000'
								txt.fill = '#000000'
				}

				txt = Magick::Draw.new

				image.annotate(txt, 0,0,35,115, "1"){
								txt.gravity = Magick::ForgetGravity
								txt.pointsize = 15
								txt.stroke = '#000000'
								txt.fill = '#000000'
				}

				txt = Magick::Draw.new

				image.annotate(txt, 0,0,40,80, "2"){
								txt.gravity = Magick::ForgetGravity
								txt.pointsize = 15
								txt.stroke = '#000000'
								txt.fill = '#000000'
				}

				txt = Magick::Draw.new

				image.annotate(txt, 0,0,60,55, "3"){
								txt.gravity = Magick::ForgetGravity
								txt.pointsize = 15
								txt.stroke = '#000000'
								txt.fill = '#000000'
				}

				txt = Magick::Draw.new

				image.annotate(txt, 0,0,95,45, "4"){
								txt.gravity = Magick::ForgetGravity
								txt.pointsize = 15
								txt.stroke = '#000000'
								txt.fill = '#000000'
				}

				txt = Magick::Draw.new

				image.annotate(txt, 0,0,130,55, "5"){
								txt.gravity = Magick::ForgetGravity
								txt.pointsize = 15
								txt.stroke = '#000000'
								txt.fill = '#000000'
				}

				txt = Magick::Draw.new

				image.annotate(txt, 0,0,155,85, "6"){
								txt.gravity = Magick::ForgetGravity
								txt.pointsize = 15
								txt.stroke = '#000000'
								txt.fill = '#000000'
				}

				txt = Magick::Draw.new

				image.annotate(txt, 0,0,155,120, "7"){
								txt.gravity = Magick::ForgetGravity
								txt.pointsize = 15
								txt.stroke = '#000000'
								txt.fill = '#000000'
				}

				txt = Magick::Draw.new

				image.annotate(txt, 0,0,135,150, "8"){
								txt.gravity = Magick::ForgetGravity
								txt.pointsize = 15
								txt.stroke = '#000000'
								txt.fill = '#000000'
				}

				txt = Magick::Draw.new

				image.annotate(txt, 0,0,45,110, "#{@cpmode.value_min} - #{@cpmode.value_max}"){
								txt.gravity = Magick::ForgetGravity
								txt.pointsize = 30 
								txt.stroke = '#0000FF'
								txt.fill = '#0000FF'
				}

			canvas.write( 'public/' + (  @cpoint.image ) )
		end

    # Use callbacks to share common setup or constraints between actions.
    def set_cpmode
    	@cpoint = Cpoint.find(params[:cpoint_id])
     	@cpmode = Cpmode.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cpmode_params
      params.require(:cpmode).permit(:cpoint_id, :date_reg, :value_min, :value_max)
    end
end
