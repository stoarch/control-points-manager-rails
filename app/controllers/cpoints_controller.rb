class CpointsController < ApplicationController
  before_action :set_cpoint, only: [:show, :edit, :update, :destroy]

  # GET /cpoints
  # GET /cpoints.json
  def index
    @cpoints = Cpoint.all
  end

  # GET /cpoints/1
  # GET /cpoints/1.json
  def show
  end

	def last_modes
		modes = @cpoint.all
    respond_to do |format|
			render status: 200, json: {
							message: "Last modes",
							modes: modes
			}.to_json
		end
	end

  # GET /cpoints/new
  def new
    @cpoint = Cpoint.new
  end

  # GET /cpoints/1/edit
  def edit
  end

  # POST /cpoints
  # POST /cpoints.json
  def create
    @cpoint = Cpoint.new(cpoint_params)

    respond_to do |format|
      if @cpoint.save
        format.html { redirect_to @cpoint, notice: 'Cpoint was successfully created.' }
        format.json { render :show, status: :created, location: @cpoint }
      else
        format.html { render :new }
        format.json { render json: @cpoint.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cpoints/1
  # PATCH/PUT /cpoints/1.json
  def update
    respond_to do |format|
      if @cpoint.update(cpoint_params)
	write_modes_image()

        format.html { redirect_to @cpoint, notice: 'Cpoint was successfully updated.' }
        format.json { render :show, status: :ok, location: @cpoint }
      else
        format.html { render :edit }
        format.json { render json: @cpoint.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cpoints/1
  # DELETE /cpoints/1.json
  def destroy
    @cpoint.destroy
    respond_to do |format|
      format.html { redirect_to cpoints_url, notice: 'Cpoint was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  	def write_modes_image()
			
		end

    # Use callbacks to share common setup or constraints between actions.
    def set_cpoint
      @cpoint = Cpoint.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cpoint_params
      params.require(:cpoint).permit(:caption, :image, :description, :code)
    end
end
