json.array!(@cpmodes) do |cpmode|
  json.extract! cpmode, :id, :cpoint_id, :date_reg, :value_min, :value_max
  json.url cpmode_url(cpmode, format: :json)
end
