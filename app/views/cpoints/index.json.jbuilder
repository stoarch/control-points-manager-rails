json.array!(@cpoints) do |cpoint|
  json.extract! cpoint, :id, :caption
  json.url cpoint_url(cpoint, format: :json)
end
