class CreateCpmodes < ActiveRecord::Migration
  def change
    create_table :cpmodes do |t|
      t.references :cpoint, index: true
      t.date :date_reg
      t.float :value_min
      t.float :value_max

      t.timestamps null: false
    end
    add_foreign_key :cpmodes, :cpoints
  end
end
