class AddFieldsToCpoint < ActiveRecord::Migration
  def change
    add_column :cpoints, :image, :string
    add_column :cpoints, :description, :string
  end
end
