require 'test_helper'

class CpointsControllerTest < ActionController::TestCase
  setup do
    @cpoint = cpoints(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:cpoints)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create cpoint" do
    assert_difference('Cpoint.count') do
      post :create, cpoint: { caption: @cpoint.caption }
    end

    assert_redirected_to cpoint_path(assigns(:cpoint))
  end

  test "should show cpoint" do
    get :show, id: @cpoint
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @cpoint
    assert_response :success
  end

  test "should update cpoint" do
    patch :update, id: @cpoint, cpoint: { caption: @cpoint.caption }
    assert_redirected_to cpoint_path(assigns(:cpoint))
  end

  test "should destroy cpoint" do
    assert_difference('Cpoint.count', -1) do
      delete :destroy, id: @cpoint
    end

    assert_redirected_to cpoints_path
  end
end
