require 'test_helper'

class CpmodesControllerTest < ActionController::TestCase
  setup do
    @cpmode = cpmodes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:cpmodes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create cpmode" do
    assert_difference('Cpmode.count') do
      post :create, cpmode: { cpoint_id: @cpmode.cpoint_id, date_reg: @cpmode.date_reg, value_max: @cpmode.value_max, value_min: @cpmode.value_min }
    end

    assert_redirected_to cpmode_path(assigns(:cpmode))
  end

  test "should show cpmode" do
    get :show, id: @cpmode
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @cpmode
    assert_response :success
  end

  test "should update cpmode" do
    patch :update, id: @cpmode, cpmode: { cpoint_id: @cpmode.cpoint_id, date_reg: @cpmode.date_reg, value_max: @cpmode.value_max, value_min: @cpmode.value_min }
    assert_redirected_to cpmode_path(assigns(:cpmode))
  end

  test "should destroy cpmode" do
    assert_difference('Cpmode.count', -1) do
      delete :destroy, id: @cpmode
    end

    assert_redirected_to cpmodes_path
  end
end
